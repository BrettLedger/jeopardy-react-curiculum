import React from 'react';
// import logo from './logo.svg';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Welcome from './components/welcome/Welcome';
import Clock from './components/clock/Clock';
import Contact from './components/contacts/Contacts';
import Navigation from './components/navigation/Navigation';
import NoMatch from './components/noMatch/NoMatch';
import Jeopardy from './components/jeopardy/Jeopardy';

function App() {
  return (
    <div className='App'>
      <Navigation />
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => <Welcome {...props} name="Kobe" />}
        />
        <Route path="/welcome/:name" component={Welcome} />
        <Route path="/clock" component={Clock} />
        <Route path="/contact" component={Contact} />
        <Route path="/jeopardy" component={Jeopardy} />
        <Route component={NoMatch} />
      </Switch> 
    </div>
  );
}

export default App;
