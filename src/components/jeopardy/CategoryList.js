import React from 'react';

function CategoryList(props){
    console.log(props)
    return(
        <div className='CategoryList'>
            {
                props.categories.map( category =>(
                <button 
                key={category.id}
                onclick ={()=> props.getQuestion(category.id,category.clues_count - 1)}
                
                >{category.title}</button>
                ))
            }
        </div>
    )

}

export default CategoryList;