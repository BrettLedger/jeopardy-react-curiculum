import React from 'react';
// import { render } from '@testing-library/react';
import AnswerForm from './AnswerForm';


function GameBoard(props) {
    const data = props.data || {}
    const value = data.value || {}
    const category = data.category || {}
    
    return(
    
        <div className='GameBoard'>
            <h2>{category.title}</h2>
            <h3>{value}</h3>
            <div className='clue'>
                {props.data.question}
            </div>
            <AnswerForm checkAnswer={props.checkAnswer}/>
            <div className='score'>
                Your Score: ${props.score}
            </div>
        </div>
    )
}

export default GameBoard;